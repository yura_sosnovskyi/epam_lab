package com.epam.spring.lab.music;

import org.springframework.context.annotation.Primary;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(2)
@Primary
public class JazzMusic implements Music{
    @Override
    public String getSong() {
        return "Louis Armstrong: What A Wonderful World";
    }

    @Override
    public String toString() {
        return "JazzMusic{" + getSong() + "}";
    }
}
