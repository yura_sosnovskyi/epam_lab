package com.epam.spring.lab.music;

import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(Ordered.HIGHEST_PRECEDENCE)
public class EdmMusic implements Music{
    @Override
    public String getSong() {
        return "Avicii: Wake Me Up";
    }

    @Override
    public String toString() {
        return "EdmMusic{" + getSong() + "}";
    }
}
