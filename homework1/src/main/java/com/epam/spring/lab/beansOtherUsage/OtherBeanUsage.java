package com.epam.spring.lab.beansOtherUsage;

import com.epam.spring.lab.beansOther.OtherBeanA;
import com.epam.spring.lab.beansOther.OtherBeanB;
import com.epam.spring.lab.beansOther.OtherBeanC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class OtherBeanUsage {
    @Autowired
    private OtherBeanA otherBeanA;
    private OtherBeanB otherBeanB;
    private OtherBeanC otherBeanC;

    @Autowired
    public OtherBeanUsage(OtherBeanB otherBeanB) {
        this.otherBeanB = otherBeanB;
    }

    @Autowired
    @Qualifier("otherBeanC")
    public void setOtherBeanC(OtherBeanC otherC) {
        this.otherBeanC = otherC;
    }

    @Override
    public String toString() {
        return "OtherBeanUsage{" +
                "otherBeanA=" + otherBeanA +
                ", otherBeanB=" + otherBeanB +
                ", otherBeanC=" + otherBeanC +
                '}';
    }
}
