package com.epam.spring.lab;

import com.epam.spring.lab.beansOtherUsage.OtherBeanUsage;
import com.epam.spring.lab.config.AppConfig1;
import com.epam.spring.lab.config.AppConfig2;
import com.epam.spring.lab.config.AppConfig3;
import com.epam.spring.lab.config.AppConfig4;
import com.epam.spring.lab.music.MusicPlayer;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Main {
    public static void main(String[] args) {
        ApplicationContext context = new AnnotationConfigApplicationContext(AppConfig1.class);
        for (String beanDefinitionName : context.getBeanDefinitionNames()) {
            System.out.println(beanDefinitionName);
        }
        System.out.println();

        ApplicationContext context2 = new AnnotationConfigApplicationContext(AppConfig2.class);
        for (String beanDefinitionName : context2.getBeanDefinitionNames()) {
            System.out.println(beanDefinitionName);
        }
        System.out.println();

        ApplicationContext context3 = new AnnotationConfigApplicationContext(AppConfig3.class);
        OtherBeanUsage otherBeanUsage = context3.getBean(OtherBeanUsage.class);
        System.out.println(otherBeanUsage + System.lineSeparator());

        ApplicationContext context4 = new AnnotationConfigApplicationContext(AppConfig4.class);
        MusicPlayer player = context4.getBean(MusicPlayer.class);
        System.out.println(player);

    }
}
