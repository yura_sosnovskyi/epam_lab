package com.epam.spring.lab.music;

import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(3)
public class RockMusic implements Music{
    @Override
    public String getSong() {
        return "Queen: We Are The Champions";
    }

    @Override
    public String toString() {
        return "RockMusic{" + getSong() + "}";
    }
}
