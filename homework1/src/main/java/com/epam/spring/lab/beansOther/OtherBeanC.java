package com.epam.spring.lab.beansOther;

import org.springframework.stereotype.Component;

@Component
public class OtherBeanC {
    @Override
    public String toString() {
        return "OtherBeanC{}";
    }
}
