package com.epam.spring.lab.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = "com.epam.spring.lab.beans1")
public class AppConfig1 {
}
