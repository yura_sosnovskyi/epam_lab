package com.epam.spring.lab.config;

import com.epam.spring.lab.beans3.BeanE;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;

@Configuration
@ComponentScan(basePackages = {"com.epam.spring.lab.beans2", "com.epam.spring.lab.beans3"},
        excludeFilters = @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, value = BeanE.class))
public class AppConfig2 {
}
