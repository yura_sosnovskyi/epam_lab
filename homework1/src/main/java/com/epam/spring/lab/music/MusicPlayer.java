package com.epam.spring.lab.music;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import java.util.List;

@Component
public class MusicPlayer {
    List<Music> musicList;
    Music favourite;
    Music downloaded;

    public MusicPlayer(List<Music> musicList,
                       @Qualifier("jazzMusic") Music favourite,
                       @Qualifier("edmMusic") Music downloaded) {
        this.musicList = musicList;
        this.favourite = favourite;
        this.downloaded = downloaded;
    }

    @Override
    public String toString() {
        return "MusicPlayer{" +
                "musicList=" + musicList +
                ",\n\t favourite=" + favourite +
                ",\n\t downloaded=" + downloaded +
                "\n}";
    }
}
