package com.epam.spring.lab.beansOther;

import org.springframework.stereotype.Component;

@Component
public class OtherBeanB {
    @Override
    public String toString() {
        return "OtherBeanB{}";
    }
}
