package com.epam.spring.lab.beansOther;

import org.springframework.stereotype.Component;

@Component
public class OtherBeanA {
    @Override
    public String toString() {
        return "OtherBeanA{}";
    }
}
