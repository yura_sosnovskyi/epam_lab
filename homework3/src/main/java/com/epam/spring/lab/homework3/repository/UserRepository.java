package com.epam.spring.lab.homework3.repository;

import com.epam.spring.lab.homework3.model.User;

public interface UserRepository {
    User getUser(String email);

    User createUser(User user);

    User updateUser(String email, User user);

    void deleteUser(String email);
}
