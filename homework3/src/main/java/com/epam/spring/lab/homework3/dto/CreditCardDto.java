package com.epam.spring.lab.homework3.dto;

import lombok.Data;

@Data
public class CreditCardDto {
    private Long id;
    private Integer balance;
    private Integer cvv;
    private Long ownerId;
}
