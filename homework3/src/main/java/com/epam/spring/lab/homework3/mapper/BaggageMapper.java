package com.epam.spring.lab.homework3.mapper;

import com.epam.spring.lab.homework3.dto.BaggageDto;
import com.epam.spring.lab.homework3.model.Baggage;
import org.mapstruct.factory.Mappers;

public interface BaggageMapper {
    BaggageMapper INSTANCE = Mappers.getMapper(BaggageMapper.class);

    BaggageDto BaggageToBaggageDto(Baggage baggage);

    Baggage BaggageDtoToBaggage(BaggageDto baggageDto);
}


