package com.epam.spring.lab.homework3.model;

import lombok.Data;

@Data
public class Check {
    private Long id;
    private Boolean paid;
    private Boolean delivered;
    private Order order;
}
