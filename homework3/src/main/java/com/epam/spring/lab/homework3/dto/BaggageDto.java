package com.epam.spring.lab.homework3.dto;

import lombok.Data;

@Data
public class BaggageDto {
    private Long id;
    private String type;
    private Integer weight;
    private Integer capacity;
    private Integer price;
    private Long orderId;
}
