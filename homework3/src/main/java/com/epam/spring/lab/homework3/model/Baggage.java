package com.epam.spring.lab.homework3.model;

import lombok.Data;

@Data
public class Baggage {
    private Long id;
    private String type;
    private Integer weight;
    private Integer capacity;
    private Integer price;
    private Long orderId;

}
