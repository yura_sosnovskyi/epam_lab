package com.epam.spring.lab.homework3.mapper;

import com.epam.spring.lab.homework3.dto.CreditCardDto;
import com.epam.spring.lab.homework3.model.CreditCard;
import org.mapstruct.factory.Mappers;

public interface CreditCardMapper {
    CreditCardMapper INSTANCE = Mappers.getMapper(CreditCardMapper.class);

    CreditCardDto CreditCardToCreditCardDto(CreditCard creditCard);

    CreditCard CreditCardDtoToCreditCard(CreditCardDto creditCardDto);
}
