package com.epam.spring.lab.homework3.model;

import lombok.Data;

@Data
public class CreditCard {
    private Long id;
    private Integer balance;
    private Integer cvv;
    private Long ownerId;
}
