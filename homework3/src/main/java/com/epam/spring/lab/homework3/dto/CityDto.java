package com.epam.spring.lab.homework3.dto;

import lombok.Data;

@Data
public class CityDto {
    private Long id;
    private String name;
    private String latitude;
    private String longitude;
}
