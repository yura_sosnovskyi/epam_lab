package com.epam.spring.lab.homework3.dto;

import lombok.Data;

import java.sql.Date;
import java.util.List;

@Data
public class OrderDto {
    private Long id;
    private Long recipientId;
    private List<BaggageDto> baggageList;
    private Integer price;
    private CityDto cityFrom;
    private String address;
    private Date receiveDate;
}
