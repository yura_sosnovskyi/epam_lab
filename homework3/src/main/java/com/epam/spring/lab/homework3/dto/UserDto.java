package com.epam.spring.lab.homework3.dto;

import lombok.Data;

import java.util.List;

@Data
public class UserDto {
    private Long id;
    private String firstName;
    private String lastName;
    private String email;
    private String password;
    private String repeatPassword;
    private Boolean isManager;
    private CreditCardDto creditCard;
    private CityDto city;
    private List<OrderDto> orderList;
}
