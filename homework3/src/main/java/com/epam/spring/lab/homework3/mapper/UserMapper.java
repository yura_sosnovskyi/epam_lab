package com.epam.spring.lab.homework3.mapper;

import com.epam.spring.lab.homework3.dto.UserDto;
import com.epam.spring.lab.homework3.model.User;
import org.mapstruct.Condition;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper
public interface UserMapper {
    UserMapper INSTANCE = Mappers.getMapper(UserMapper.class);

    @Condition
    default boolean isSuitable(UserDto userDto) {
        if (userDto.getPassword() == null
                || userDto.getPassword().length() < 1
                || !userDto.getPassword().equals(userDto.getRepeatPassword()))
            throw new IllegalArgumentException("Passwords are not equal!");
        return true;
    }

    @Mapping(ignore = true, target = "password")
    UserDto UserToUserDto(User user);

    @Mapping(target = "password", conditionExpression = "java(isSuitable(userDto))")
    User UserDtoToUser(UserDto userDto);
}
