package com.epam.spring.lab.homework4.repository.impl;

import com.epam.spring.lab.homework4.exception.UserAlreadyExistsException;
import com.epam.spring.lab.homework4.exception.UserNotFoundException;
import com.epam.spring.lab.homework4.model.User;
import com.epam.spring.lab.homework4.repository.UserRepository;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class UserRepositoryImpl implements UserRepository {

    private final List<User> list = new ArrayList<>();

    @Override
    public User getUser(String email) {
        return list.stream()
                .filter(user -> user.getEmail().equals(email))
                .findFirst()
                .orElseThrow(UserNotFoundException::new);
    }

    @Override
    public User createUser(User user) {
        if (list.stream().anyMatch(oldUser ->
                oldUser.getEmail().equals(user.getEmail()))) {
            throw new UserAlreadyExistsException();
        }
        list.add(user);
        return user;
    }

    @Override
    public User updateUser(String email, User user) {
        boolean isDeleted = list.removeIf(u -> u.getEmail().equals(email));
        if (isDeleted) {
            list.add(user);
        } else {
            throw new UserNotFoundException();
        }
        return user;
    }

    @Override
    public void deleteUser(String email) {
        list.removeIf(user -> user.getEmail().equals(email));
    }

}



