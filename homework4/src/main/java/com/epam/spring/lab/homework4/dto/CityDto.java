package com.epam.spring.lab.homework4.dto;

import com.epam.spring.lab.homework4.dto.group.OnCreate;
import com.epam.spring.lab.homework4.dto.group.OnUpdate;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;
import javax.validation.constraints.Positive;

@Data
public class CityDto {
    @Null(message = "'id' of city should be absent in request", groups = OnUpdate.class)
    @NotNull(message = "'id' of city shouldn't be null or empty", groups = OnCreate.class)
    @Positive(message = "'id' of city must be bigger than zero", groups = OnCreate.class)
    private Long id;

    @NotBlank(message = "'name' of city shouldn't be empty", groups = OnCreate.class)
    private String name;

    @NotBlank(message = "'latitude' shouldn't be empty", groups = OnCreate.class)
    private String latitude;

    @NotBlank(message = "'longitude' shouldn't be empty", groups = OnCreate.class)
    private String longitude;
}
