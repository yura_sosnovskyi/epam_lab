package com.epam.spring.lab.homework4.service.impl;

import com.epam.spring.lab.homework4.dto.UserDto;
import com.epam.spring.lab.homework4.mapper.UserMapper;
import com.epam.spring.lab.homework4.model.User;
import com.epam.spring.lab.homework4.repository.UserRepository;
import com.epam.spring.lab.homework4.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    @Override
    public UserDto getUser(String email) {
        log.info("getUser by email {}", email);
        User user = userRepository.getUser(email);
        return UserMapper.INSTANCE.UserToUserDto(user);
    }

    @Override
    public UserDto createUser(UserDto userDto) {
        log.info("createUser with email {}", userDto.getEmail());
        User user = UserMapper.INSTANCE.UserDtoToUser(userDto);
        user = userRepository.createUser(user);
        return UserMapper.INSTANCE.UserToUserDto(user);
    }

    @Override
    public UserDto updateUser(String email, UserDto userDto) {
        log.info("updateUser with email {}", email);
        User user = UserMapper.INSTANCE.UserDtoToUser(userDto);

        User oldUser = userRepository.getUser(email);
        user.setEmail(oldUser.getEmail());
        user.setPassword(oldUser.getPassword());
        user.setId(oldUser.getId());
        user.setIsManager(oldUser.getIsManager());

        user = userRepository.updateUser(email, user);
        return UserMapper.INSTANCE.UserToUserDto(user);
    }

    @Override
    public void deleteUser(String email) {
        log.info("deleteUser with email {}", email);
        userRepository.deleteUser(email);
    }
}
