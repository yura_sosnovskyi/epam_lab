package com.epam.spring.lab.homework4.mapper;

import com.epam.spring.lab.homework4.dto.BaggageDto;
import com.epam.spring.lab.homework4.model.Baggage;
import org.mapstruct.factory.Mappers;

public interface BaggageMapper {
    BaggageMapper INSTANCE = Mappers.getMapper(BaggageMapper.class);

    BaggageDto BaggageToBaggageDto(Baggage baggage);

    Baggage BaggageDtoToBaggage(BaggageDto baggageDto);
}


