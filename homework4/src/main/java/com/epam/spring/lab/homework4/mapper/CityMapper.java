package com.epam.spring.lab.homework4.mapper;

import com.epam.spring.lab.homework4.dto.CityDto;
import com.epam.spring.lab.homework4.model.City;
import org.mapstruct.factory.Mappers;

public interface CityMapper {
    CityMapper INSTANCE = Mappers.getMapper(CityMapper.class);

    CityDto CityToCityDto(City city);

    City CityDtoToCity(CityDto cityDto);
}
