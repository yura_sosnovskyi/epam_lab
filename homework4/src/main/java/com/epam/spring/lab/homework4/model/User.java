package com.epam.spring.lab.homework4.model;

import lombok.Data;

import java.util.List;

@Data
public class User {
    private Long id;
    private String firstName;
    private String lastName;
    private String email;
    private String password;
    private Boolean isManager;
    private CreditCard creditCard;
    private City city;
    private List<Order> orderList;
}
