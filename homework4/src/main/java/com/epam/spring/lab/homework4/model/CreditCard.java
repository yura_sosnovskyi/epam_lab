package com.epam.spring.lab.homework4.model;

import lombok.Data;

@Data
public class CreditCard {
    private Long id;
    private Double balance;
    private Integer cvv;
    private Long ownerId;
}
