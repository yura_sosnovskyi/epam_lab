package com.epam.spring.lab.homework4.dto;

import com.epam.spring.lab.homework4.dto.group.OnCreate;
import com.epam.spring.lab.homework4.dto.group.OnUpdate;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;
import javax.validation.constraints.Positive;

@Data
public class CreditCardDto {

    @Null(message = "'id' should be absent in request", groups = OnUpdate.class)
    @NotNull(message = "'id' shouldn't be null or empty", groups = OnCreate.class)
    @Positive(message = "'id' must be bigger than zero", groups = OnCreate.class)
    private Long id;

    @Null(message = "'ownerId' should be absent in request", groups = OnUpdate.class)
    @NotNull(message = "'ownerId' shouldn't be null or empty", groups = OnCreate.class)
    @Positive(message = "'ownerId' must be bigger than zero", groups = OnCreate.class)
    private Long ownerId;

    @NotBlank(message = "'balance' shouldn't be empty", groups = OnCreate.class)
    private Double balance;

    @NotNull(message = "'cvv' shouldn't be null or empty", groups = OnCreate.class)
    @Positive(message = "'cvv' should be bigger than zero", groups = OnCreate.class)
    private Integer cvv;
}
