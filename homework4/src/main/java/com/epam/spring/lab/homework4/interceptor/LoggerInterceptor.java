package com.epam.spring.lab.homework4.interceptor;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Slf4j
public class LoggerInterceptor implements HandlerInterceptor {
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {
        log.info("[preHandle][request: " + request + "] [request method: " + request.getMethod()
                + "] [request URI: " + request.getRequestURI() + "] [user session id: "
                + request.getSession().getId() + "]");

        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) {
        log.info("[postHandle][request: " + request + "] [request method: " + request.getMethod()
                + "] [request URI: " + request.getRequestURI() + "] [user session id: "
                + request.getSession().getId() + "]");
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) {
        log.info("[afterCompletion][request: " + request + "] [request method: " + request.getMethod()
                + "] [request URI: " + request.getRequestURI() + "] [user session id: "
                + request.getSession().getId() + "]");
    }
}
