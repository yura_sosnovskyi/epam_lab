package com.epam.spring.lab.homework4.repository;

import com.epam.spring.lab.homework4.model.User;

public interface UserRepository {
    User getUser(String email);

    User createUser(User user);

    User updateUser(String email, User user);

    void deleteUser(String email);
}
