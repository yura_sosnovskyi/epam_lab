package com.epam.spring.lab.homework4.model;

import lombok.Data;
import lombok.ToString;

import java.util.List;

@Data
@ToString(onlyExplicitlyIncluded = true)
public class City {
    private static List<City> cities;
    private Long id;
    @ToString.Include
    private String name;
    private String latitude;
    private String longitude;
}
