package com.epam.spring.lab.homework4.dto;

import com.epam.spring.lab.homework4.dto.group.OnCreate;
import com.epam.spring.lab.homework4.dto.group.OnUpdate;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;
import javax.validation.constraints.Positive;

@Data
public class BaggageDto {
    @Null(message = "'id' of baggage should be absent in request", groups = OnUpdate.class)
    @NotNull(message = "'id' of baggage shouldn't be empty", groups = OnCreate.class)
    @Positive(message = "'id' of baggage must be bigger than zero", groups = OnCreate.class)
    private Long id;

    @Null(message = "'orderId' of baggage should be absent in request", groups = OnUpdate.class)
    @NotNull(message = "'orderId' of baggage shouldn't be null or empty", groups = OnCreate.class)
    @Positive(message = "'orderId' of baggage must be bigger than zero", groups = OnCreate.class)
    private Long orderId;

    @NotBlank(message = "'type' shouldn't be empty", groups = OnCreate.class)
    private String type;

    @NotNull(message = "'weight' shouldn't be null or empty", groups = OnCreate.class)
    @Positive(message = "'weight' should be bigger than zero", groups = OnCreate.class)
    private Integer weight;

    @NotNull(message = "'capacity' shouldn't be null or empty", groups = OnCreate.class)
    @Positive(message = "'capacity' should be bigger than zero", groups = OnCreate.class)
    private Integer capacity;

    @NotNull(message = "'price' shouldn't be null or empty", groups = OnCreate.class)
    @Positive(message = "'price' should be bigger than zero", groups = OnCreate.class)
    private Integer price;
}
