package com.epam.spring.lab.homework4.model;

import lombok.Data;

import java.sql.Date;
import java.util.List;

@Data
public class Order {
    private Long id;
    private Long recipientId;
    private List<Baggage> baggageList;
    private Integer price;
    private City cityFrom;
    private String address;
    private Date receiveDate;
}
