package com.epam.spring.lab.homework5.controller;

import com.epam.spring.lab.homework5.controller.assembler.UserAssembler;
import com.epam.spring.lab.homework5.controller.assembler.UserListAssembler;
import com.epam.spring.lab.homework5.controller.model.UserModel;
import com.epam.spring.lab.homework5.dto.UserDto;
import com.epam.spring.lab.homework5.service.UserService;
import com.epam.spring.lab.homework5.test.config.TestConfig;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import static com.epam.spring.lab.homework5.test.util.TestDataUtil.TEST_EMAIL;
import static com.epam.spring.lab.homework5.test.util.TestDataUtil.createUserDto;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ExtendWith(SpringExtension.class)
@WebMvcTest(UserController.class)
@Import(TestConfig.class)
class UserControllerTest {

    @MockBean
    private UserService userService;

    @MockBean
    private UserAssembler userAssembler;

    @MockBean
    private UserListAssembler userListAssembler;

    @Autowired
    private MockMvc mockMvc;

    @Test
    void getUserTest() throws Exception {
        UserDto userDto = createUserDto();
        UserModel userModel = new UserModel(userDto);

        when(userService.getUser(TEST_EMAIL)).thenReturn(userDto);
        when(userAssembler.toModel(userDto)).thenReturn(userModel);

        mockMvc.perform(get("/api/v1/users/" + TEST_EMAIL))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.email").value(TEST_EMAIL));
    }

}