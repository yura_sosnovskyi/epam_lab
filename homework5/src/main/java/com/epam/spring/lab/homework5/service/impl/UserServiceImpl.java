package com.epam.spring.lab.homework5.service.impl;

import com.epam.spring.lab.homework5.dto.UserDto;
import com.epam.spring.lab.homework5.exception.UserAlreadyExistsException;
import com.epam.spring.lab.homework5.exception.UserNotFoundException;
import com.epam.spring.lab.homework5.mapper.UserMapper;
import com.epam.spring.lab.homework5.model.User;
import com.epam.spring.lab.homework5.repository.UserRepository;
import com.epam.spring.lab.homework5.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    @Override
    public UserDto getUser(String email) {
        log.info("getUser by email {}", email);
        User user = userRepository.findByEmail(email)
                .orElseThrow(UserNotFoundException::new);
        log.info("User with {} email is successfully found", email);
        return UserMapper.INSTANCE.UserToUserDto(user);
    }

    @Override
    @Transactional
    public UserDto createUser(UserDto userDto) {
        log.info("createUser with {} email...", userDto.getEmail());
        if (userRepository.existsByEmail(userDto.getEmail())) {
            throw new UserAlreadyExistsException();
        }

        User user = UserMapper.INSTANCE.UserDtoToUser(userDto);
        user = userRepository.save(user);

        log.info("user with {} email, {} id is successfully created", user.getEmail(), user.getId());
        return UserMapper.INSTANCE.UserToUserDto(user);
    }

    @Override
    @Transactional
    public UserDto updateUser(String email, UserDto userDto) {
        log.info("updateUser with email {}", email);
        User user = UserMapper.INSTANCE.UserDtoToUser(userDto);

        User oldUser = userRepository.findByEmail(email)
                .orElseThrow(UserNotFoundException::new);
        setOldProperties(user, oldUser);

        user = userRepository.save(user);
        log.info("user with {} email is successfully updated", user.getEmail());
        return UserMapper.INSTANCE.UserToUserDto(user);
    }

    @Override
    @Transactional
    public void deleteUser(String email) {
        log.info("deleteUser with email {}", email);
        User user = userRepository.findByEmail(email)
                .orElseThrow(UserNotFoundException::new);
        userRepository.delete(user);
        log.info("user with {} email is successfully deleted", email);
    }

    @Override
    public List<UserDto> getUsers(String sortBy, Integer pageNumber, Integer pageSize) {
        log.info("getUsers sorted by {} on pageNumber {} with pageSize {}",
                sortBy, pageNumber, pageSize);
        Pageable pageable = PageRequest.of(pageNumber, pageSize, Sort.by(sortBy));
        List<User> list = userRepository.findUsersBy(pageable);
        if (list.size() == 0) {
            throw new UserNotFoundException();
        }

        log.info("users sorted by {} on pageNumber {} with pageSize {} are found",
                sortBy, pageNumber, pageSize);
        return list.stream()
                .map(UserMapper.INSTANCE::UserToUserDto)
                .collect(Collectors.toList());

    }

    @Override
    public List<UserDto> getUsers() {
        log.info("getUsers sorted by city names");
        List<UserDto> list = userRepository.findUsersOrderByCityNameAsc()
                .stream().map(UserMapper.INSTANCE::UserToUserDto)
                .collect(Collectors.toList());
        if (list.size() == 0) {
            throw new UserNotFoundException();
        }

        log.info("users sorted by city names are found");
        return list;
    }

    private void setOldProperties(User user, User oldUser) {
        user.setEmail(oldUser.getEmail());
        user.setPassword(oldUser.getPassword());
        user.setId(oldUser.getId());
        user.setIsManager(oldUser.getIsManager());
        if (user.getCreditCard() == null)
            user.setCreditCard(oldUser.getCreditCard());
        if (user.getCity() == null)
            user.setCity(oldUser.getCity());
        if (user.getOrderList() == null)
            user.setOrderList(oldUser.getOrderList());
    }
}
