package com.epam.spring.lab.homework5.service;

import software.amazon.awssdk.services.s3.model.PutObjectRequest;
import com.epam.spring.lab.homework5.dto.UserDto;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import software.amazon.awssdk.core.sync.RequestBody;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.model.PutObjectResponse;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

@Slf4j
@Component
@RequiredArgsConstructor
public class S3JobRunner implements ApplicationRunner {

    private final S3Client s3Client;
    private final UserService userService;
    private final int delay = 5000;
    private final int period = 3_600_000;
    private final String fileNamePattern = "userList-%d";
    private final String bucketName = "lpnu-core-events-processed-us-east-1-tf-test";

    private void sendBatchOfMessages() {
        try {
            ObjectMapper objectMapper = new ObjectMapper().enable(SerializationFeature.INDENT_OUTPUT);
            List<UserDto> userList = userService.getUsers();
            String content = objectMapper.writeValueAsString(userList);
            String objectName = String.format(fileNamePattern, userList.size());

            PutObjectRequest putOb = PutObjectRequest.builder()
                    .bucket(bucketName)
                    .key(objectName)
                    .build();

            s3Client.putObject(putOb,
                    RequestBody.fromString(content));

            log.info("Object {} created in s3 bucket, containing {} messages:\n{}", objectName, userList.size(), content);
        } catch (JsonProcessingException e) {
            log.warn(e.getMessage());
        }
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {
        new Timer().scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                sendBatchOfMessages();
            }
        }, delay, period);
    }
}
