package com.epam.spring.lab.homework5.service;

import com.epam.spring.lab.homework5.dto.UserDto;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface UserService {
    UserDto getUser(String email);

    UserDto createUser(UserDto userDto);

    UserDto updateUser(String email, UserDto userDto);

    void deleteUser(String email);

    List<UserDto> getUsers(String sortBy, Integer pageNumber, Integer pageSize);

    @Transactional
    List<UserDto> getUsers();

}
