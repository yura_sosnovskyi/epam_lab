package com.epam.spring.lab.homework5.api;

import com.epam.spring.lab.homework5.controller.model.UserListModel;
import com.epam.spring.lab.homework5.controller.model.UserModel;
import com.epam.spring.lab.homework5.dto.UserDto;
import com.epam.spring.lab.homework5.dto.group.OnCreate;
import com.epam.spring.lab.homework5.dto.group.OnUpdate;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@Api(tags = "User management API")
@RequestMapping("/api/v1/users")
public interface UserApi {

    @ApiImplicitParams({
            @ApiImplicitParam(name = "email", paramType = "path", required = true, value = "User email"),
    })
    @ApiOperation("Get user")
    @ResponseStatus(HttpStatus.OK)
    @GetMapping(value = "/{email}")
    UserModel getUser(@PathVariable String email);

    @ApiImplicitParams({
            @ApiImplicitParam(name = "sortBy", paramType = "query",
                    allowableValues = "id, firstName, lastName, email",
                    defaultValue = "id", value = "Sort type"),

            @ApiImplicitParam(name = "pageNumber", paramType = "query",
                    defaultValue = "0", value = "Page number"),

            @ApiImplicitParam(name = "pageSize", paramType = "query",
                    defaultValue = "5", value = "Page size")
    })
    @ApiOperation("Get sorted and paginated users")
    @ResponseStatus(HttpStatus.OK)
    @GetMapping
    UserListModel getUsers(@RequestParam(name = "sortBy", required = false,
                                         defaultValue = "firstName") String sortBy,
                           @RequestParam(name = "pageNumber", required = false,
                                         defaultValue = "0") Integer page,
                           @RequestParam(name = "pageSize", required = false,
                                         defaultValue = "5") Integer pageSize
    );

    @ApiOperation("Get users sorted by city names")
    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/all_sorted")
    UserListModel getUsers();

    @ApiOperation("Create user")
    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping
    UserModel createUser(@RequestBody @Validated(OnCreate.class) UserDto userDto);

    @ApiImplicitParams({
            @ApiImplicitParam(name = "email", paramType = "path", required = true, value = "User email"),
    })
    @ApiOperation("Update user")
    @ResponseStatus(HttpStatus.OK)
    @PatchMapping(value = "/{email}")
    UserModel updateUser(@PathVariable String email, @RequestBody @Validated(OnUpdate.class) UserDto userDto);

    @ApiImplicitParams({
            @ApiImplicitParam(name = "email", paramType = "path", required = true, value = "User email"),
    })
    @ApiOperation("Delete user")
    @DeleteMapping(value = "/{email}")
    ResponseEntity<Void> deleteUser(@PathVariable String email);

}
