package com.epam.spring.lab.homework5.dto;

import com.epam.spring.lab.homework5.dto.group.OnCreate;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class CityDto {

    private Long id;

    @NotBlank(message = "'name' of city shouldn't be empty", groups = OnCreate.class)
    private String name;

    @NotBlank(message = "'latitude' shouldn't be empty", groups = OnCreate.class)
    private String latitude;

    @NotBlank(message = "'longitude' shouldn't be empty", groups = OnCreate.class)
    private String longitude;
}
