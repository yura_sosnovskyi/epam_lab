package com.epam.spring.lab.homework5.model.enums;

public enum ErrorType {
    VALIDATION_ERROR_TYPE,
    PROCESSING_ERROR_TYPE,
    DATABASE_ERROR_TYPE,
    FATAL_ERROR_TYPE
}
