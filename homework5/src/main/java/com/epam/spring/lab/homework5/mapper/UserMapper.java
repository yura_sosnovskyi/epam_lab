package com.epam.spring.lab.homework5.mapper;

import com.epam.spring.lab.homework5.dto.UserDto;
import com.epam.spring.lab.homework5.model.User;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper
public interface UserMapper {
    UserMapper INSTANCE = Mappers.getMapper(UserMapper.class);

    @Mapping(ignore = true, target = "password")
    UserDto UserToUserDto(User user);

    User UserDtoToUser(UserDto userDto);
}
