package com.epam.spring.lab.homework5.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import software.amazon.awssdk.auth.credentials.ProfileCredentialsProvider;
import software.amazon.awssdk.profiles.ProfileFile;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.s3.S3Client;

@Configuration
public class S3Config {

    @Bean("s3Client")
    public S3Client s3Client() {
        return S3Client.builder().credentialsProvider(
                ProfileCredentialsProvider.builder()
                        .profileFile(ProfileFile.defaultProfileFile()).build())
                .region(Region.US_EAST_1)
                .build();
    }
}
