package com.epam.spring.lab.homework5.mapper;

import com.epam.spring.lab.homework5.dto.CreditCardDto;
import com.epam.spring.lab.homework5.model.CreditCard;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;


public interface CreditCardMapper {
    CreditCardMapper INSTANCE = Mappers.getMapper(CreditCardMapper.class);

    CreditCardDto CreditCardToCreditCardDto(CreditCard creditCard);

    CreditCard CreditCardDtoToCreditCard(CreditCardDto creditCardDto);
}
