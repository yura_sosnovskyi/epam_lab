package com.epam.spring.lab.homework5.mapper;

import com.epam.spring.lab.homework5.dto.OrderDto;
import com.epam.spring.lab.homework5.model.Order;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface OrderMapper {
    OrderMapper INSTANCE = Mappers.getMapper(OrderMapper.class);

    OrderDto OrderToOrderDto(Order order);

    Order OrderDtoToOrder(OrderDto orderDto);
}
