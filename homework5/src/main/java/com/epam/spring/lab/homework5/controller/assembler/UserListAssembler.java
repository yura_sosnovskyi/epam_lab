package com.epam.spring.lab.homework5.controller.assembler;

import com.epam.spring.lab.homework5.controller.UserController;
import com.epam.spring.lab.homework5.controller.model.UserListModel;
import com.epam.spring.lab.homework5.dto.UserDto;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import org.springframework.stereotype.Component;

import java.util.List;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@Component
public class UserListAssembler extends RepresentationModelAssemblerSupport<List<UserDto>, UserListModel> {

    public static final String GET_REL = "get_user";
    public static final String CREATE_REL = "create_user";
    public static final String UPDATE_USER = "update_user";
    public static final String DELETE_USER = "delete_user";
    public static final String GET_PAGINATED_USERS = "get_paginated_users";
    public static final String GET_ALL_USERS_SORTED_BY_CITY = "get_all_users_sorted_by_city";
    public static final String sortBy = "firstName";
    public static final int pageNumber = 0;
    public static final int pageSize = 5;

    public UserListAssembler() {
        super(UserController.class, UserListModel.class);
    }

    @Override
    public UserListModel toModel(List<UserDto> entityList) {
        UserListModel userListModel = new UserListModel(entityList);

        Link get = linkTo(methodOn(UserController.class)
                .getUser(entityList.get(0).getEmail())).withRel(GET_REL);
        Link create = linkTo(methodOn(UserController.class)
                .createUser(entityList.get(0))).withRel(CREATE_REL);
        Link update = linkTo(methodOn(UserController.class)
                .updateUser(entityList.get(0).getEmail(), entityList.get(0))).withRel(UPDATE_USER);
        Link delete = linkTo(methodOn(UserController.class)
                .deleteUser(entityList.get(0).getEmail())).withRel(DELETE_USER);
        Link getPaginated = linkTo(methodOn(UserController.class)
                .getUsers(sortBy, pageNumber, pageSize)).withRel(GET_PAGINATED_USERS);
        Link getAll = linkTo(methodOn(UserController.class).getUsers()).withRel(GET_ALL_USERS_SORTED_BY_CITY);

        userListModel.add(get, create, update, delete, getPaginated, getAll);

        return userListModel;
    }

}
