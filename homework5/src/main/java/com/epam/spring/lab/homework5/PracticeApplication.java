package com.epam.spring.lab.homework5;

import com.epam.spring.lab.homework5.dto.UserDto;
import com.epam.spring.lab.homework5.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;

@Slf4j
@SpringBootApplication
public class PracticeApplication {

    public static void main(String[] args) {
        SpringApplication.run(PracticeApplication.class, args);
    }

    @Profile("test")
    @Bean
    public CommandLineRunner demoUser(UserService userService,
                                      @Value("${info.app.user.password}") String password,
                                      @Value("${info.app.user.email}") String email) {
        return args -> {
            UserDto userDto = UserDto.builder()
                    .firstName("TestUserF")
                    .lastName("TestUserL")
                    .email(email)
                    .password(password)
                    .repeatPassword(password)
                    .build();
            log.info("Creating default user with email: {}", email);
            userService.createUser(userDto);
        };
    }

}
