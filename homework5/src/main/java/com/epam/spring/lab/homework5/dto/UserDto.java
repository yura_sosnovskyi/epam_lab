package com.epam.spring.lab.homework5.dto;

import com.epam.spring.lab.homework5.dto.group.OnCreate;
import com.epam.spring.lab.homework5.dto.group.OnUpdate;
import com.epam.spring.lab.homework5.validator.EqualFields;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.*;
import java.util.List;

@Data
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
@EqualFields(baseField = "password", matchField = "repeatPassword", groups = OnCreate.class)
public class UserDto {

    private Long id;

    @NotBlank(message = "{firstName.message}", groups = OnCreate.class)
    private String firstName;

    @NotBlank(message = "{firstName.message}", groups = OnCreate.class)
    private String lastName;

    @Email
    @Null(message = "{email.update.message}", groups = OnUpdate.class)
    @NotBlank(message = "{email.create.message}", groups = OnCreate.class)
    private String email;

    @Null(message = "{password.update.message}", groups = OnUpdate.class)
    @NotBlank(message = "{password.create.message}", groups = OnCreate.class)
    private String password;

    @Null(message = "{repeatPassword.update.message}", groups = OnUpdate.class)
    @NotBlank(message = "{repeatPassword.create.message}", groups = OnCreate.class)
    private String repeatPassword;

    @Null(message = "{isManager.update.message}", groups = OnUpdate.class)
    @NotNull(message = "{isManager.create.message}", groups = OnCreate.class)
    private Boolean isManager;

    @NotNull(message = "{creditCard.message}", groups = OnCreate.class)
    private CreditCardDto creditCard;

    @NotNull(message = "{city.message}", groups = OnCreate.class)
    private CityDto city;

    @NotNull(message = "{orderList.message}", groups = OnCreate.class)
    private List<OrderDto> orderList;

}
