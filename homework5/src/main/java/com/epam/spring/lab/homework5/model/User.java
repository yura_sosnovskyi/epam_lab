package com.epam.spring.lab.homework5.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Data
@Builder
@Entity
@NoArgsConstructor
@AllArgsConstructor
@NamedQuery(name = "User.findUsersOrderByCityNameAsc",
        query = "select u from User u order by u.city.name asc")

public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String firstName;
    private String lastName;

    @Column(nullable = false, unique = true)
    private String email;
    private String password;
    private Boolean isManager;

    @OneToOne(cascade = {CascadeType.ALL}, orphanRemoval = true)
    @JoinColumn
    private CreditCard creditCard;

    @OneToOne(cascade = {CascadeType.ALL}, orphanRemoval = true)
    @JoinColumn
    private City city;

    @OneToMany(cascade = {CascadeType.ALL}, orphanRemoval = true)
    private List<Order> orderList;
}


