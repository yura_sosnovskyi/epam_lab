package com.epam.spring.lab.homework5.dto;

import com.epam.spring.lab.homework5.dto.group.OnCreate;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

@Data
public class BaggageDto {

    private Long id;

    @NotBlank(message = "'type' shouldn't be empty", groups = OnCreate.class)
    private String type;

    @NotNull(message = "'weight' shouldn't be null or empty", groups = OnCreate.class)
    @Positive(message = "'weight' should be bigger than zero", groups = OnCreate.class)
    private Integer weight;

    @NotNull(message = "'capacity' shouldn't be null or empty", groups = OnCreate.class)
    @Positive(message = "'capacity' should be bigger than zero", groups = OnCreate.class)
    private Integer capacity;

    @NotNull(message = "'price' shouldn't be null or empty", groups = OnCreate.class)
    @Positive(message = "'price' should be bigger than zero", groups = OnCreate.class)
    private Integer price;
}
