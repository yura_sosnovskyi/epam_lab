package com.epam.spring.lab.homework5.dto;

import com.epam.spring.lab.homework5.dto.group.OnCreate;
import lombok.Data;

import javax.validation.constraints.*;
import java.sql.Date;
import java.util.List;

@Data
public class OrderDto {

    private Long id;

    @NotEmpty(message = "'baggageList' shouldn't be empty", groups = OnCreate.class)
    private List<BaggageDto> baggageList;

    @Positive(message = "'price' should be bigger than zero", groups = OnCreate.class)
    private Integer price;

    @NotNull(message = "'cityFrom' shouldn't be empty", groups = OnCreate.class)
    private CityDto cityFrom;

    @NotBlank(message = "'address' shouldn't be empty", groups = OnCreate.class)
    private String address;

    @FutureOrPresent(message = "'receiveDate' should be in future or present", groups = OnCreate.class)
    private Date receiveDate;
}
