package com.epam.spring.lab.homework5.dto;

import com.epam.spring.lab.homework5.dto.group.OnCreate;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

@Data
public class CreditCardDto {

    private Long id;

    @NotBlank(message = "'balance' shouldn't be empty", groups = OnCreate.class)
    private Double balance;

    @NotNull(message = "'cvv' shouldn't be null or empty", groups = OnCreate.class)
    @Positive(message = "'cvv' should be bigger than zero", groups = OnCreate.class)
    private Integer cvv;
}
