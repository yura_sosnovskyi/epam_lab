package com.epam.spring.lab.homework5.controller.model;

import com.epam.spring.lab.homework5.dto.UserDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.hateoas.RepresentationModel;

import java.util.List;

@Data
@EqualsAndHashCode(callSuper = false)
@AllArgsConstructor
@NoArgsConstructor
public class UserListModel extends RepresentationModel<UserListModel> {

    private List<UserDto> userDtoList;

}