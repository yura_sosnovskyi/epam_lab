package com.epam.spring.lab.homework5.controller;

import com.epam.spring.lab.homework5.api.UserApi;
import com.epam.spring.lab.homework5.controller.assembler.UserAssembler;
import com.epam.spring.lab.homework5.controller.assembler.UserListAssembler;
import com.epam.spring.lab.homework5.controller.model.UserListModel;
import com.epam.spring.lab.homework5.controller.model.UserModel;
import com.epam.spring.lab.homework5.dto.UserDto;
import com.epam.spring.lab.homework5.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class UserController implements UserApi {

    private final UserService userService;
    private final UserAssembler userAssembler;
    private final UserListAssembler userListAssembler;

    @Override
    public UserModel getUser(String email) {
        UserDto outUserDto = userService.getUser(email);
        return userAssembler.toModel(outUserDto);
    }

    @Override
    public UserListModel getUsers(String sortBy,
                                  Integer pageNumber, Integer pageSize) {
        List<UserDto> userDtoList = userService.getUsers(sortBy, pageNumber, pageSize);
        return userListAssembler.toModel(userDtoList);
    }

    @Override
    public UserListModel getUsers() {
        return userListAssembler.toModel(userService.getUsers());
    }

    @Override
    public UserModel createUser(UserDto userDto) {
        UserDto outUserDto = userService.createUser(userDto);
        return userAssembler.toModel(outUserDto);
    }

    @Override
    public UserModel updateUser(String email, UserDto userDto) {
        UserDto outUserDto = userService.updateUser(email, userDto);
        return userAssembler.toModel(outUserDto);
    }

    @Override
    public ResponseEntity<Void> deleteUser(String email) {
        userService.deleteUser(email);
        return ResponseEntity.noContent().build();
    }

}
