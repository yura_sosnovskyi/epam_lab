package com.epam.spring.lab.beans;

import org.springframework.beans.factory.BeanInitializationException;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class BeanA implements InitializingBean, DisposableBean, BeanValidator {
    private String name;
    private int value;

    @Value("BEAN_A")
    public void setName(String name) {
        this.name = name;
    }

    @Value("99")
    public void setValue(int value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "BeanA{" +
                "name='" + name + '\'' +
                ", value=" + value +
                '}';
    }

    @Override
    public void destroy() {
        System.out.println("DisposableBean.destroy() for beanA");
    }

    @Override
    public void afterPropertiesSet() {
        System.out.println("InitializingBean.afterPropertiesSet() for beanA");
    }

    @Override
    public void validate(String beanName) {
        if (name == null) {
            throw new BeanInitializationException(beanName + " name cannot be equal to null!");
        }

        if (value < 0) {
            throw new BeanInitializationException(beanName + " value must be equal or bigger than zero!");
        }
    }
}
