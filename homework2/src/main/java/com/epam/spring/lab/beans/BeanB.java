package com.epam.spring.lab.beans;

import org.springframework.beans.factory.BeanInitializationException;
import org.springframework.beans.factory.annotation.Value;

public class BeanB implements BeanValidator {
    private String name;
    private int value;

    @Value("${beanB.name}")
    public void setName(String name) {
        this.name = name;
    }

    @Value("${beanB.value}")
    public void setValue(int value) {
        this.value = value;
    }

    public void initMethod() {
        System.out.println("initMethod for beanB");
    }

    public void newInitMethod() {
        System.out.println("newInitMethod for beanB");
    }

    public void destroyMethod() {
        System.out.println("destroyMethod for beanB");
    }

    @Override
    public String toString() {
        return "BeanB{" +
                "name='" + name + '\'' +
                ", value=" + value +
                '}';
    }

    @Override
    public void validate(String beanName) {
        if (name == null) {
            throw new BeanInitializationException(beanName + " name cannot be equal to null!");
        }

        if (value < 0) {
            throw new BeanInitializationException(beanName + " value must be equal or bigger than zero!");
        }
    }
}
