package com.epam.spring.lab.beans;

import org.springframework.beans.factory.BeanInitializationException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

@Lazy
@Component
public class BeanF implements BeanValidator {
    private String name;
    private int value;

    @Value("BEAN_F")
    public void setName(String name) {
        this.name = name;
    }

    @Value("2")
    public void setValue(int value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "BeanF{" +
                "name='" + name + '\'' +
                ", value=" + value +
                '}';
    }

    @Override
    public void validate(String beanName) {
        if (name == null) {
            throw new BeanInitializationException(beanName + " name cannot be equal to null!");
        }

        if (value < 0) {
            throw new BeanInitializationException(beanName + " value must be equal or bigger than zero!");
        }
    }
}
