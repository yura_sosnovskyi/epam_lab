package com.epam.spring.lab.config;

import com.epam.spring.lab.beans.BeanB;
import com.epam.spring.lab.beans.BeanC;
import com.epam.spring.lab.beans.BeanD;
import org.springframework.context.annotation.*;

@Configuration
@PropertySource("classpath:app.properties")
public class AppConfig1 {
    @Bean(initMethod = "initMethod", destroyMethod = "destroyMethod")
    @DependsOn("beanD")
    public BeanB beanB() {
        return new BeanB();
    }

    @Bean(initMethod = "initMethod", destroyMethod = "destroyMethod")
    @DependsOn("beanB")
    public BeanC beanC() {
        return new BeanC();
    }

    @Bean(initMethod = "initMethod", destroyMethod = "destroyMethod")
    public BeanD beanD() {
        return new BeanD();
    }
}
