package com.epam.spring.lab.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@ComponentScan("com.epam.spring.lab.beans")
@Import(AppConfig1.class)
public class AppConfig2 {
}
