package com.epam.spring.lab;

import com.epam.spring.lab.beans.BeanA;
import com.epam.spring.lab.beans.BeanE;
import com.epam.spring.lab.config.AppConfig2;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Lazy;


public class Main {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(AppConfig2.class);
        System.out.println(System.lineSeparator() + context.getBean(BeanA.class));
        System.out.println(context.getBean(BeanE.class));
        System.out.println(context.getBeansWithAnnotation(Bean.class));
        System.out.println(context.getBeansWithAnnotation(Lazy.class) + System.lineSeparator());
        context.close();
    }
}
