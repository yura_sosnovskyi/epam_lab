package com.epam.spring.lab.beans;

public interface BeanValidator {
    void validate(String beanName);
}
