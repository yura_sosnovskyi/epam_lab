package com.epam.spring.lab.beans;

import org.springframework.beans.factory.BeanInitializationException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

@Component
public class BeanE implements BeanValidator {
    private String name;
    private int value;

    @Value("BEAN_E")
    public void setName(String name) {
        this.name = name;
    }

    @Value("58")
    public void setValue(int value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "BeanE{" +
                "name='" + name + '\'' +
                ", value=" + value +
                '}';
    }

    @PreDestroy
    public void preDestroy() {
        System.out.println("@PreDestroy method for beanE");
    }

    @PostConstruct
    public void postConstruct() {
        System.out.println("@PostConstruct method for beanE");
    }

    @Override
    public void validate(String beanName) {
        if (name == null) {
            throw new BeanInitializationException(beanName + " name cannot be equal to null!");
        }

        if (value < 0) {
            throw new BeanInitializationException(beanName + " value must be equal or bigger than zero!");
        }
    }
}
